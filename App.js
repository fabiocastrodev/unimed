import {
  createAppContainer,
  createStackNavigator,
  createDrawerNavigator
} from 'react-navigation'

import Bootstrap from './screens/Bootstrap'

import Login from './screens/Login'

import Home from './screens/Home'
import Checkup from './screens/Checkup'

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: Home
  },
  Checkup: {
    screen: Checkup
  }
})

const AppNavigator = createStackNavigator({
  Bootstrap: {
    screen: Bootstrap
  },
  Login: {
    screen: Login
  },
  Main: {
    screen: DrawerNavigator
  }
}, {
  headerMode: 'none'
})

export default createAppContainer(AppNavigator)