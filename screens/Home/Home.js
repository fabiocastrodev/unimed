import React from 'react'

import {
  View,
  Text,

  Image
} from 'react-native'

import DrawerView from '../DrawerView'

import styles from './styles'

import Ripple from 'react-native-material-ripple'

const stories = [
  {
    active: true,
    source: require('../../assets/story_1.png')
  },
  {
    source: require('../../assets/story_1.png')
  },
  {
    source: require('../../assets/story_1.png')
  },
  {
    source: require('../../assets/story_1.png')
  }
]

class Home extends React.PureComponent {
  static navigationOptions = {
    drawerLabel: 'Início'
  }

  constructor(props) {
    super(props)

    this.state = {
      size: null
    }

    this.onLayout = this.onLayout.bind(this)

    this.navigateCheckup = () => this.props.navigation.navigate('Checkup')
  }

  onLayout(e) {
    const { layout: { width }} = e.nativeEvent

    if (!this.state.size) {
      this.setState({ size: width / 2 })
    }
  }

  render() {
    const { size } = this.state

    const width = size ? size - 10 : '100%'
    const height = size ? size - 10 : 0

    const buttonStyle = {
      width,
      height
    }

    return (
      <DrawerView
        header={(
          <>
            <View style={styles.logoContainer}>
              <Image
                style={styles.logo}
                resizeMode="contain"
                source={require('../../assets/logo.png')}
              />
            </View>
            <View style={{ width: 44, height: 44, }}></View>
          </>
        )}
      >
        <View style={{ flex: 1 }}>
          <Text style={styles.text}>
            Novos vídeos
          </Text>

          <View style={styles.storiesContainer}>
            {stories.map((s, i) => (
              <View style={{ marginRight: 10 }} key={i}>
                <Ripple rippleContainerBorderRadius={32} rippleCentered>
                  <View style={[styles.story, s.active ? styles.activeStory : null]}>
                    <View style={styles.storyContainer}>
                      <Image source={s.source} style={styles.storyImg} resizeMode="stretch" />
                    </View>
                  </View>
                </Ripple>
              </View>
            ))}
          </View>

          <Text style={styles.title}>
            Quem ama <Text style={styles.titleBold}>cuida</Text>
          </Text>
        </View>
        <View style={styles.actions}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View onLayout={this.onLayout} style={[styles.button, buttonStyle, { backgroundColor: '#6a3061' }]}>
              <Ripple rippleContainerBorderRadius={10}>
                <View style={{ width: '100%', height: '100%', padding: 15, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require('../../assets/medicamentos.png')} style={{ height: '30%', marginBottom: 10 }} resizeMode="contain" />

                  <Text style={{ color: '#fff', fontFamily: 'Proxima Nova', fontSize: 18, textAlign: 'center' }}>Medicamentos</Text>
                </View>
              </Ripple>
            </View>

            <View style={[styles.button, buttonStyle, { backgroundColor: '#ec1651' }]}>
              <Ripple rippleContainerBorderRadius={10} onPress={this.navigateCheckup}>
                <View style={{ width: '100%', height: '100%', padding: 15, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require('../../assets/checkup.png')} style={{ height: '30%', marginBottom: 10 }} resizeMode="contain" />

                  <Text style={{ color: '#fff', fontFamily: 'Proxima Nova', fontSize: 18, textAlign: 'center' }}>Check Up Diário</Text>
                </View>
              </Ripple>
            </View>
          </View>

          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={[styles.button, buttonStyle, { backgroundColor: '#0e57ed' }]}>
              <Ripple rippleContainerBorderRadius={10}>
                <View style={{ width: '100%', height: '100%', padding: 15, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require('../../assets/glicemia_PA.png')} style={{ height: '30%', marginBottom: 10 }} resizeMode="contain" />

                  <Text style={{ color: '#fff', fontFamily: 'Proxima Nova', fontSize: 18, textAlign: 'center' }}>Glicemia e PA</Text>
                </View>
              </Ripple>
            </View>

            <View style={[styles.button, buttonStyle, { backgroundColor: '#ffca05' }]}>
              <Ripple rippleContainerBorderRadius={10}>
                <View style={{ width: '100%', height: '100%', padding: 15, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require('../../assets/dicas_cuidados.png')} style={{ height: '30%', marginBottom: 10 }} resizeMode="contain" />

                  <Text style={{ color: '#fff', fontFamily: 'Proxima Nova', fontSize: 18, textAlign: 'center' }}>Dicas e Cuidados</Text>
                </View>
              </Ripple>
            </View>
          </View>
        </View>
      </DrawerView>
    )
  }
}

export default Home