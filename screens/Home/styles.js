import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    flex: 1,

    backgroundColor: '#00985c'
  },

  content: {
    flex: 1,

    paddingBottom: 0,
    paddingHorizontal: 25,
  },

  menu: {
    alignItems: 'center',
    justifyContent: 'center',

    padding: 10,
    borderRadius: 5
  },

  logo: {
    height: 88,
    maxWidth: '80%'
  },

  logoContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 10
  },

  text: {
    fontFamily: 'Proxima Nova',
    color: '#fff'
  },

  story: {
    width: 64,
    height: 64,
    
    borderWidth: 2,
    borderColor: '#00995c',

    borderRadius: 32,

    overflow: 'hidden'
  },

  storyContainer: {
    width: '100%',
    height: '100%',
    borderWidth: 2,
    borderColor: '#00995c',
  },

  storyImg: {
    width: '100%',
    height: '100%'
  },

  activeStory: {
    borderWidth: 2,
    borderColor: '#b2d347'
  },

  storiesContainer: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row'
  },

  title: {
    fontFamily: 'Proxima Nova',
    fontWeight: '300',
    fontSize: 24,
    color: '#fff'
  },

  titleBold: {
    fontWeight: 'bold',
    color: '#b2d347'
  },

  actions: {
    flex: 2
  },

  button: {
    borderRadius: 10
  }
})