import React from 'react'

import {
  View,
  Image,

  Text
} from 'react-native'

import Ripple from 'react-native-material-ripple'

const icons = [
  require('../../assets/1.png'),
  require('../../assets/2.png'),
  require('../../assets/3.png'),
  require('../../assets/4.png'),
  require('../../assets/5.png'),
]

class Checkup extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      selected: 1
    }
  }

  render() {
    const icon = {
      borderRadius: 100,
      borderColor: 'transparent',
      borderWidth: 10,
    }

    const selectedStyle = {
      borderColor: '#fff'
    }

    return (
      <View style={{ flex: 1, backgroundColor: '#ed0e50', padding: 25 }}>
        <Text style={{ marginTop: 40, marginBottom: 20, color: '#fff', fontFamily: 'Proxima Nova', fontSize: 60 }}>
          Como você percebe <Text style={{ fontWeight: 'bold' }}>sua saúde hoje?</Text>
        </Text>

        <Text style={{ color: '#fff', fontFamily: 'Proxima Nova', fontSize: 20, marginBottom: 10 }}>
          Conte-nos mais sobre sua saúde
        </Text>

        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'flex-start', justifyContent: 'space-between' }}>
          {icons.map((src, i) => (
            <Ripple key={i} onPress={() => this.setState({ selected: i })}>
              <View style={[icon, i === this.state.selected ? selectedStyle : null]}>
                <Image source={src} style={{ width: 40, height: 40 }} resizeMode="stretch"/>
              </View>
            </Ripple>
          ))}
        </View>

        <View>
          <Ripple onPress={this.navigate} rippleContainerBorderRadius={25}>
            <View style={{ padding: 15, borderColor: '#fff', borderWidth: 1, borderRadius: 25, backgroundColor: 'transparent' }}>
              <Text style={{ fontSize: 18, color: '#fff', fontFamily: 'Proxima Nova', fontWeight: 'bold', textAlign: 'center'}}>Enviar</Text>
            </View>
          </Ripple>
        </View>
      </View>
    )
  }
}

export default Checkup