import React from 'react'
import PropTypes from 'prop-types'

import {
  View
} from 'react-native'

import {
  Constants
} from 'expo'

import { MaterialIcons } from '@expo/vector-icons'
import Ripple from 'react-native-material-ripple'

import { withNavigation } from 'react-navigation'

import styles from './styles'

class DrawerView extends React.PureComponent {
  constructor(props) {
    super(props)

    this.openDrawer = () => this.props.navigation.openDrawer()
  }

  render() {
    const { children, header } = this.props

    return (
      <View style={styles.root}>
        <View style={{ height: Constants.statusBarHeight }}></View>

        <View style={styles.content}>
          <View style={styles.header}>
            <View style={styles.menuContainer}>
              <Ripple onPress={this.openDrawer} rippleContainerBorderRadius={5} rippleCentered>
                <View style={styles.menu}>
                  <MaterialIcons name="menu" size={24} color="#fff" />
                </View>
              </Ripple>
            </View>

            {header}
          </View>

          <View style={styles.children}>
            {children}
          </View>
        </View>
      </View>
    )
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,

    header: PropTypes.node,
    children: PropTypes.node
  }
}

export default withNavigation(DrawerView)