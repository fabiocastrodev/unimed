import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    flex: 1,

    backgroundColor: '#00985c'
  },

  content: {
    flex: 1,

    paddingVertical: 10,
    paddingHorizontal: 20,
  },

  menu: {
    alignItems: 'center',
    justifyContent: 'center',

    padding: 10,
    borderRadius: 22
  },

  menuContainer: {
    height: 44,
    width: 44
  },

  children: {
    flex: 1
  },

  header: {
    flexDirection: 'row',
  }
})