import React from 'react'
import PropTypes from 'prop-types'

import {
  StatusBar
} from 'react-native'

import {
  Font,

  AppLoading,
  SplashScreen 
} from 'expo'

class Bootstrap extends React.Component {
  constructor(props) {
    super(props)

    this.loadFonts = this.loadFonts.bind(this)
    this.loadAllAssets = this.loadAllAssets.bind(this)
  }

  async componentDidMount() {
    const { navigation } = this.props

    StatusBar.setBarStyle('light-content')

    await this.loadAllAssets()

    SplashScreen.hide()

    navigation.navigate('Main')
  }

  loadFonts() {
    return Font.loadAsync({
      'Proxima Nova': require('../../assets/ProximaNova.ttf')
    })
  }

  loadAllAssets() {
    const all = Promise.all([
      this.loadFonts()
    ])

    return all
  }

  render() {
    return (
      <AppLoading autoHideSplash={false} />
    )
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired
  }
}

export default Bootstrap