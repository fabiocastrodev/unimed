import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  Image,

  Text,
  KeyboardAvoidingView,

  Keyboard,
  TouchableWithoutFeedback
} from 'react-native'

import Ripple from 'react-native-material-ripple'
import { TextField } from 'react-native-material-textfield'

import styles from './styles'

class Login extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      login: '',
      password: ''
    }

    this.onChangeLogin = this.onChange.bind(this, 'login')
    this.onChangePassword = this.onChange.bind(this, 'password')

    this.navigate = () => this.props.navigation.navigate('Main')
  }

  onChange(name, text) {
    this.setState({
      [name]: text
    })
  }
  
  render() {
    const { login, password } = this.state

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.root}>
          <View style={styles.content}>
            <View style={styles.center}>
              <Image
                style={styles.logo}
                resizeMode="contain"
                source={require('../../assets/logo.png')}
              />

              <Text style={styles.title}>
                Faça o <Text style={styles.bold}>login</Text> com sua conta
              </Text>
            </View>

            <KeyboardAvoidingView behavior="padding">
              <View>
                <TextField
                  label="Seu ID"
                  textColor="rgba(255, 255, 255, .87)"
                  tintColor="rgba(255, 255, 255, 1)"
                  baseColor="rgba(255, 255, 255, .87)"

                  value={login}
                  onChangeText={this.onChangeLogin}
                />

                <TextField
                  label="Senha"
                  secureTextEntry
                  textColor="rgba(255, 255, 255, .87)"
                  tintColor="rgba(255, 255, 255, 1)"
                  baseColor="rgba(255, 255, 255, .87)"

                  value={password}
                  onChangeText={this.onChangePassword}
                />
              </View>
            </KeyboardAvoidingView>

            <View>
              <View style={styles.actionContainer}>
                <Ripple onPress={this.navigate} rippleContainerBorderRadius={25}>
                  <View style={styles.button}>
                    <Text style={styles.buttonText}>Entrar</Text>
                  </View>
                </Ripple>
              </View>

              <Text style={styles.action}>
                Esqueceu sua senha?
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired
  }
}

export default Login