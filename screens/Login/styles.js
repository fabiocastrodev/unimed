import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    flex: 1,
    padding: 25,
    backgroundColor: '#421664',
    justifyContent: 'center'
  },
  content: {
    width: '100%',
    height: '80%',
    justifyContent: 'flex-start'
  },

  logo: {
    width: '70%'
  },

  title: {
    fontSize: 18,
    color: '#fff',
  },
  bold: {
    fontWeight: 'bold'
  },

  action: {
    fontSize: 16,
    fontFamily: 'Proxima Nova',
    color: '#fff',
    textAlign: 'center'
  },

  actionContainer: {
    marginTop: 25,
    marginBottom: 20
  },

  button: {
    padding: 15,

    borderRadius: 25,

    backgroundColor: 'rgba(0, 152, 92, 1)',
  },

  buttonText: {
    fontSize: 18,
    color: '#fff',
    fontFamily: 'Proxima Nova',
    fontWeight: 'bold',
    textAlign: 'center'
  },

  center: {
    flexDirection: 'column',
    alignItems: 'center'
  }
})